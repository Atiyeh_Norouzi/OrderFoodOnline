create table User
( 
	 id  int not null,
	 username  varchar(30) not null,
	 familyname varchar(30) not null,
	 phonenumber int ,
	 email varchar(50) not null,
	 pass varchar(20) not null,
	 credit int ,
	 experience int,
     cardnumber int,
     height int,
     weight int ,
     BMI int,
	 primary key(id)
 );
create table Address
 (
	 id int not null,
	 addresstitle varchar(30),
	 useraddress varchar(100),
	 primary key(id)
 );
create table UserhasAddress
(
	  userid int not null,
	  addressid int not null,
	  foreign key(userid) references User(id),
	  foreign key(addressid) references Address(id)
);
create table facility
(
	id int not null,
    facilityname varchar(30),
	primary key(id)
);

create table restaurant
(
   id int not null,
   fpname varchar(30) not null,
   rate int default 0,
   address varchar(100),
   image varchar(200),
   timetosendOrder int default 12,
   facilityid int ,
   restaurnatOwnerid int not null,
   primary key (id),
   foreign key(restaurnatOwnerid) references User(id),
   foreign key(facilityid) references facility(id)
);
create table UserhasFavRestaurants
(
	userid int not null,
    restaurantid int not null,
    foreign key(userid) references User(id),
    foreign key(restaurantid) references Restaurant(id)
);
create table restaurantCategory
(
	id int not null,
    categoryname varchar(20),
    primary key(id)
);   
create table restaurantHasCategory
(
	restaurantid int not null,
	categoryid int not null,
	foreign key(restaurantid) references restaurant(id),
	foreign key(categoryid) references restaurantCategory(id)
);
create table fooditem
(
	id int not null,
    foodname varchar(30) not null,
    price int default 0,
    image varchar(200),
    foodDescription varchar(300),
    primary key(id)
);
create table restaurantHasItems
(
	restaurantid int not null,
    fooditemid int not null,
    foreign key(restaurantid) references restaurant(id),
    foreign key(fooditemid) references fooditem(id)
);
create table ingredience
(
	id int not null,
    iname varchar(20),
    primary key(id)
);
create table itemHasIngrediences
(
	itemid int not null,
    ingredienceid int not null,
    foreign key(itemid) references fooditem(id),
    foreign key(ingredenceid) references ingredence(id)
);
create table nutritionchart
(
	id int not null,
    nutritionname varchar(30) not null,
    primary key (id)
);
create table itemHasNutritionValue
(
	itemid int not null,
    nutritionid int not null,
    nutritionvalue int not null,
    foreign key (itemid) references fooditem(id),
    foreign key (nutritionid) references nutritionchart(id)
);
create table fooditemCategory
(
	id int not null,
    categoryname varchar(20),
    primary key(id)
);  
create table itemHasCategory
(
	itemid int not null,
	categoryid int not null,
	foreign key(itemid) references fooditem(id),
	foreign key(categoryid) references fooditemCategory(id)
);
create table plan
(
	id int not null,
	foodonday varchar(30)  not null,
    day varchar(20) not null,
    primary key(id)
);
Create table itemHasPlans
(
	itemid int not null,
    planid int not null,
    foreign key(itemid) references fooditem(id),
	foreign key(planid) references plan(id)
);
create table OrderFood
(
	id int not null,
    FoodProvidername varchar(30) not null,
    orderdate date not null,
    codeRahgiri varchar(30) not null,
    userid int not null ,
    deliverstate varchar(30) not null,
    timetodeliver varchar(20) not null,
    totalPrice int not null,
    primary key(id),
    foreign key(userid) references User(id)
);
create table orderHasItems
(
	orderid int not null,
    itemid int not null,
    foreign key(orderid) references OrderFood(id),
    foreign key (itemid) references fooditem(id)
);
create table survey
(
	id int not null,
	userid int not null,
    surveyQuestion varchar(300),
    primary key(id)
);
create table surveyoption
(
	id int not null,
    surveyoptiontitle varchar(200),
    primary key(id)
);
create table surveyHasOptions
(
	surveyid int not null,
    optionid int not null,
    foreign key(surveyid) references survey(id),
    foreign key (optionid) references surveyoption(id)
);
create table comment
(
	id int not null,
    content varchar(300),
    userid int not null,
    createddate date not null,
    primary key(id),
    foreign key(userid) references User(id)
);
create table surveyHasComments
(
	surveyid int not null,
    commentid int not null,
    foreign key ( surveyid) references Survey(id),
    foreign key(commentid) references Comment(id)
);
create table reward
(
	id int not null,
    rewardtitle int not null,
    rewardcontent varchar(200),
    rewardimage varchar(300),
    primary key(id)
);
create table userHasRewards
(
	userid int not null,
    rewardid int not null,
	foreign key (userid) references User(id),
    foreign key(rewardid) references reward(id)
);
create table payment
(
	id int not null,
    userid int not null,
    orderid int not null,
    paymentdate int not null,
    primary key(id),
	foreign key (userid) references User(id),
    foreign key(orderid) references orderfood(id)	
);
