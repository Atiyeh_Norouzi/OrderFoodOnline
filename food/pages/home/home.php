<!doctype html>
<html>
<?php
session_start();
$restaurantOwner;
$link = mysqli_connect("localhost", "root", "", "orderfoodonline");

// Check connection
if($link === false){

    die("ERROR: Could not connect. " . mysqli_connect_error());

}   
  if(isset($_SESSION['credit']))
  {
  $credit = $_SESSION["credit"];
  }
  if(isset($_SESSION['username']) && !empty($_SESSION['username']))
  {
  $username = $_SESSION["username"];
  }
  if(isset($_SESSION['familyname']) && !empty($_SESSION['familyname']))
  {
  $familyname = $_SESSION["familyname"];
  }
  if(isset($_SESSION['id']) && !empty($_SESSION['id']))
  {
  $id = $_SESSION["id"];
      // Prepare a select statement
      $sql = "SELECT resturauntOwner FROM person WHERE id = $id";
        
      if($stmt = mysqli_prepare($link, $sql)){

          // Attempt to execute the prepared statement
          if(mysqli_stmt_execute($stmt)){
              // Store result
              mysqli_stmt_store_result($stmt);
              
              if(mysqli_stmt_num_rows($stmt) == 1){                    
                  if(mysqli_stmt_fetch($stmt)){
      
                          if($result = mysqli_query($link, $sql))
                          {  
                              if (mysqli_num_rows($result) > 0) {
                                      // output data of each row
                                      $row = mysqli_fetch_assoc($result);
                                      $restaurantOwner =  $row["resturauntOwner"];      
                              
                              } else {
                                  echo "0 results";
                              }
                          }
                      }
                  }
              } 
          } else{
              echo "Oops! Something went wrong. Please try again later.";
          }
          mysqli_stmt_close($stmt);
  }
  //post
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $add=$_POST['address'];
    $rest=$_POST['restaurant'];
    $_SESSION['address'] =  $add;  
    $_SESSION['rest'] = $rest;
    ?>
    <script>
            location.href = '../../listing.php';
    </script>
    <?php

  } 
  mysqli_close($link);
?>

<head>
  <meta charset="utf-8">
  <title>food</title>
  <link href='https://fonts.googleapis.com/css?family=Lobster+Two:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,300,700' rel='stylesheet' type='text/css' />

  <!--MOBILE DEVICE-->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

  <!--CSS-->
  <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="../../css/style.css" />
  <link rel="stylesheet" type="text/css" href="../../css/font-awesome.css" />
  <link rel="stylesheet" type="text/css" href="../../css/animate.css">
  <link rel="stylesheet" type="text/css" href="../../css/responsive.css" />
  <link rel="stylesheet" href="../../css/owl.carousel.min.css">
  <link rel="stylesheet" href="../../css/themify-icons.css">
  <link rel="stylesheet" href="../../css/atiyehnorouzzadeh.css">
  <link rel="stylesheet" href="../../css/dorsasamiee.css">
  <!--JS-->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <!-- jQuery and Bootstrap -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAK3nwDhmzXhT_7p5LP-s9JsSMWSDz9xYk&c&libraries=places&sensor=false"
        type="text/javascript"></script>

  <script type="text/javascript" src="../../js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="../../js/modernizr.js"></script>
  <script src="../../js/scripts.js"></script>
  <script src="../../js/waypoints.min.js"></script>
  <script src = "../../js/owl.carousel.min.js"></script>
  <script src = "../../js/homeController.js"></script>

</head>

<body>

  <!-- Paste this code after body tag -->
  <div class="se-pre-con"></div>
  <!-- Ends -->
 <div class="container col-md-12 searchsection">
        <div class="header-content col-md-12">
            <div class="header  col-md-offset-2 col-md-4">
                    <?php
                if(isset($_SESSION['username']) && !empty($_SESSION['username']))
                { ?>
                <span id="name">Hello <?php echo $username ?>! </span>
                <?php } ?>
                <a id="logo" href="../../images/logo1.png"></a>
             
            </div>
            <div class="header-bar col-md-4" id = "header">
                <div class="Header_rider ">
                    <a class="rider_btn  textbox os-animation" data-os-animation="zoomIn" data-os-animation-delay="0.5s" href="#">
                        <span class="rider_span">
                            <svg height="24" width="24" viewbox="0 0 24 24" class="rider_svg">
                                <path d=" M4.98374048,11.1033469 L4.97784415,11.101078 L6.55697395,6.99728298 L5.05809227,7.00182646
                        L5.05202981,5.00183565 L8.01192238, 4.99286348 L8.94824228,6.35198597 L8.69888814,7 L15.373489,7 L15.1576503,5.80951949
                        C15.0714007,5.33380104 14.6571653,4.98791504 14.1736914, 4.98791504 L12.0089795,4.98791504 L12.0089795,2.98791504
                        L14.1736914,2.98791504 C15.6241132,2.98791504 16.8668192,4.02557304 17.125568,5.45272838 L18.1316178,
                        11.0016987 C20.8322058,11.0714834 23,13.2825839 23,16 C23,18.7614237 20.7614237,21 18,21 C15.2385763,21
                        13,18.7614237 13,16 C13,13.8875735 14.309993 ,12.0811124 16.1619206,11.3486748 L15.8913097,9.85609288
                        L10.5503712,13.9248342 C10.8391035,14.5569363 11,15.2596785 11,16 C11,18.7614237 8.76142375, 21 6,21
                        C3.23857625,21 1,18.7614237 1,16 C1,13.58679 2.70960594,11.5728798 4.98374048,11.1033469 Z M7.11194952,11.1240883
                        C7.97016726,11.3189845 8.74450611, 11.7346496 9.37209566,12.3082135 L13.7147077,9 L7.92929327,9 L7.11194952,11.1240883
                        Z M6,19 C7.65685425,19 9,17.6568542 9,16 C9,14.3431458 7.65685425,13 6,13 C4.34314575, 13 3,14.3431458
                        3,16 C3,17.6568542 4.34314575,19 6,19 Z M18,19 C19.6568542,19 21,17.6568542 21,16 C21,14.3431458 19.6568542,13
                        18,13 C16.3431458,13 15,14.3431458 15,16 C15, 17.6568542 16.3431458, 19 18,19 Z "></path>
                            </svg>
                            <span>Become a Rider</span>
                        </span>
                    </a>
                </div>

                     <div class="Header_rider">
                    <a class="rider_btn  textbox os-animation"  data-os-animation="zoomIn" id = "login" data-os-animation-delay="0.75s"  <?php
                                        if(!isset($_SESSION['username']) && empty($_SESSION['username']))
                                        { ?> href = "../login/login.php " <?php } ?> >
                                            
                                                <span class="rider_span" >
                                                    <?php
                                        if(isset($_SESSION['username']) && !empty($_SESSION['username']))
                                        { ?><script> new openProfile(); </script>
                                        <i class="user-circle"></i>
                                                Hi  <?php echo $username ?> <?php } else
                                        {?>
                                                Login
                                        
                                        <?php } ?>
                                      </span>  
                                        </a>
                                      
                                      </div>
                <div class="Header_rider ">
         
                    <a class="rider_btn textbox os-animation" data-os-animation="zoomIn" data-os-animation-delay="1s" href="../register/register.php">
                        <span class="rider_span">
                            <span>Register</span>
                        </span>
                    </a>
                </div>
             
            </div>


        </div>
<div class="container-fluid">
        <div class="content col-md-12">
            <div class="col-md-offset-2 col-md-8">

                <div class="col-md-12 animation">
                    <div class="burger-img">
                             <div class="profile_box hideProfile" style = "display : inline-block; position:absolute; right:2%;">
                            <img src = "../../images/avatar.png" > 
                            <p><?php echo $username ." ".$familyname ?></p>
                            <p>Credit: <?php echo $credit   ?></p>
                            <a
                             href=<?php if ($restaurantOwner == 0){ ?>"../../Theme/index.html"<?php }else{ ?>"../../material-lite/lite/index.html" <?php } ?>>Profile</a>
                            <a href="../register/code-exec.php" > Logout </a> 
                            </div> 
                    </div>
                    <h4 class="frame-1 ">ATIIDOR</h4>
                    <h4 class="frame-2 ">Order food online!</h4>
                    <h4 class="frame-3 ">We find your diet:)</h4>

                    <h4 class="frame-4 ">
                        with
                        <i class="fa fa-heartbeat "></i>
                    </h4>

                    <h4 class="frame-5">
                        <span>ATIDOR</span>
                        <span>Atiyeh</span>
                        <span>Dorsa</span>
                    </h4>
                </div>
                <div class="col-md-12 ">
                    <form class="form-inline" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method = "POST">
                        <div class="input-group ">
                            <input id="address" type="text" class="form-control " name="address" placeholder="Address">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-map-marker"></i>
                            </span>
                        </div>
                        <div class="input-group">
                            <input id="food" type="text" class="form-control" name="text" placeholder="Food">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-cutlery"></i>
                            </span>
                        </div>
                        <div class="input-group">
                            <input id="restaurant" type="text" class="form-control" name="restaurant" placeholder="Restaurant">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-home"></i>
                            </span>
                        </div>    
                        <div class="input-group">
                            <button type="submit"  name = "submit" value = "submit" class="btn btn-default searchbutton"  >
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>
                                        </div>
 

  <section class="saction3">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="ordaring">
            <h2 class="os-animation" data-os-animation="zoomIn" data-os-animation-delay="0.50s">Ordering food was never so easy</h2>
            <div class="dotted os-animation" data-os-animation="bounceInLeft" data-os-animation-delay="1s"></div>
            <p class="os-animation" data-os-animation="zoomIn" data-os-animation-delay="0.50s">Just 4 Steps </p>
            <div class="dotted1 os-animation" data-os-animation="bounceInRight" data-os-animation-delay="1s"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6">
          <figure class="step os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.50s">
            <img src="../../images/one.png" alt="" /> </figure>
          <div class="arrow">
            <img src="../../images/arrow.png" alt="" /> </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
          <figure class="step os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="1.5s">
            <img src="../../images/two.png" alt="" /> </figure>
          <div class="arrow1 ">
            <img src="../../images/arrow.png" alt="" /> </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
          <figure class="step os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="2.5s">
            <img src="../../images/thrww.png" alt="" /> </figure>
          <div class="arrow">
            <img src="../../images/arrow.png" alt="" /> </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
          <figure class="step1 os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="3.5s">
            <img src="../../images/four.png" alt="" /> </figure>
        </div>
      </div>
    </div>
  </section>
  <section class="saction4">
        <div class="section light-bg" id="gallery">
        <div class="container">
            <div class="section-title">
           
                <h3 style= "color:#633991">Earn Sweet Rewards</h3>
            </div>

            <div class="img-gallery owl-carousel owl-theme">
              
                <img src="../../images/screen1.png" alt="image">
                <img src="../../images/screen2.jpg" alt="image">
                <img src="../../images/screen3.jpg" alt="image">
                <img src="../../images/screen4.jpg" alt="image">
            </div>

        </div>

    </div>
  </section>
  <section class="saction5">
    <div class="container" id="resturant">
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="restaurants">
          <h3 class="os-animation" data-os-animation="rollIn" data-os-animation-delay="1s">Top Restaurant </h3>
        </div>
        <div class="dotted6 os-animation" data-os-animation="bounceInRight" data-os-animation-delay="0.50s"></div>
        <div class="row">
          <div class="col-lg-6 col-md-8 col-sm-8 col-xs-6">
            <figure class="rest os-animation" data-os-animation="fadeInDown" data-os-animation-delay="1s">
              <a href="#">
                <img src="../../images/pizzhut.png" alt="" /> </a>
            </figure>
          </div>
          <div class="col-lg-6 col-md-8 col-sm-8 col-xs-6">
            <figure class="rest os-animation" data-os-animation="fadeInDown" data-os-animation-delay="1.2s">
              <a href="#">
                <img src="../../images/SUB.png" alt="" />
              </a>
            </figure>
          </div>
        </div>
        <!--row-->

        <div class="row">
          <div class="col-lg-6 col-md-8 col-sm-8 col-xs-6">
            <figure class="rest os-animation" data-os-animation="fadeInDown" data-os-animation-delay="1.4s">
              <a href="#">
                <img src="../../images/KFC.png" alt="" />
              </a>
            </figure>
          </div>
          <div class="col-lg-6 col-md-8 col-sm-8 col-xs-6">
            <figure class="rest os-animation" data-os-animation="fadeInDown" data-os-animation-delay="1.6s">
              <a href="#">
                <img src="../../images/papjohns.png" alt="" />
              </a>
            </figure>
          </div>
        </div>
        <!--row-->

        <div class="row">
          <div class="col-lg-6 col-md-8 col-sm-8 col-xs-6">
            <figure class="rest os-animation" data-os-animation="fadeInDown" data-os-animation-delay="1.8s">
              <a href="#">
                <img src="../../images/dominos.png" alt="" />
              </a>
            </figure>
          </div>
          <div class="col-lg-6 col-md-8 col-sm-8 col-xs-6">
            <figure class="rest os-animation" data-os-animation="fadeInDown" data-os-animation-delay="2s">
              <a href="#">
                <img src="../../images/barista.png" alt="" />
              </a>
            </figure>
          </div>
        </div>
        <!--row-->

      </div>
      <!---col-->

      <div class="col-lg-8 col-md-8 col-sm-8">
        <div class="food">
          <h3 class="os-animation" data-os-animation="rollIn" data-os-animation-delay="2.5s">Top Foods</h3>
        </div>
        <div class="dotted7 os-animation" data-os-animation="bounceInRight" data-os-animation-delay="2.8s"></div>
        <div class="food1">
          <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <figure class="food os-animation" data-os-animation="fadeInDown
" data-os-animation-delay="3s">
                <img src="../../images/pizz.png" alt="" />
                <div class="order">
                  <a href="#">food name</a>
                </div>
              </figure>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <figure class="food os-animation" data-os-animation="fadeInDown
" data-os-animation-delay="3.2s">
                <img src="../../images/burgar.png" alt="" />
                <div class="order">
                  <a href="#">food name</a>
                </div>
              </figure>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <figure class="food os-animation" data-os-animation="fadeInDown
" data-os-animation-delay="3.4s">
                <img src="../../images/donelt.png" alt="" />
                <div class="order">
                  <a href="#">food name</a>
                </div>
              </figure>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <figure class="food os-animation" data-os-animation="fadeInDown
" data-os-animation-delay="3.6s">
                <img src="../../images/sup.png" alt="" />
                <div class="order">
                  <a href="#">food name</a>
                </div>
              </figure>
            </div>
          </div>
        </div>
        <div class="food1">
          <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <figure class="food os-animation" data-os-animation="fadeInDown
" data-os-animation-delay="3.8s">
                <img src="../../images/checken.png" alt="" />
                <div class="order">
                  <a href="#">food name</a>
                </div>
              </figure>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <figure class="food os-animation" data-os-animation="fadeInDown
" data-os-animation-delay="4s">
                <img src="../../images/passta.png" alt="" />
                <div class="order">
                  <a href="#">food name</a>
                </div>
              </figure>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <figure class="food os-animation" data-os-animation="fadeInDown
" data-os-animation-delay="4.2s">
                <img src="../../images/bhel.png" alt="" />
                <div class="order">
                  <a href="#">food name</a>
                </div>
              </figure>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <figure class="food os-animation" data-os-animation="fadeInDown
" data-os-animation-delay="4.4s">
                <img src="../../images/past.png" alt="" />
                <div class="order">
                  <a href="#">food name</a>
                </div>
              </figure>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>




    <!-- ***** Cool Facts Area Start ***** -->
    <section class="cool_facts_area clearfix">
        <div class="container">
            <div class="row">
                <!-- Single Cool Fact-->
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.2s">
                        <div class="counter-area">
                            <h3>
                                <span class="counter">90</span>
                            </h3>
                        </div>
                        <div class="cool-facts-content">
                            <i class="ion-arrow-down-a"></i>
                            <p>APP
                                <br> DOWNLOADS</p>
                        </div>
                    </div>
                </div>
                <!-- Single Cool Fact-->
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="counter-area">
                            <h3>
                                <span class="counter">120</span>
                            </h3>
                        </div>
                        <div class="cool-facts-content">
                            <i class="ion-happy-outline"></i>
                            <p>Happy
                                <br> Clients</p>
                        </div>
                    </div>
                </div>
                <!-- Single Cool Fact-->
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.6s">
                        <div class="counter-area">
                            <h3>
                                <span class="counter">40</span>
                            </h3>
                        </div>
                        <div class="cool-facts-content">
                            <i class="ion-person"></i>
                            <p>ACTIVE
                                <br>ACCOUNTS</p>
                        </div>
                    </div>
                </div>
                <!-- Single Cool Fact-->
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.8s">
                        <div class="counter-area">
                            <h3>
                                <span class="counter">10</span>
                            </h3>
                        </div>
                        <div class="cool-facts-content">
                            <i class="ion-ios-star-outline"></i>
                            <p>TOTAL
                                <br>APP RATES</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Cool Facts Area End ***** -->

    <div class="section light-bg" id="features">


        <div class="container">

            <div class="section-title">
         
                <h3 style= "color:#00aaa7">Features you love</h3>
            </div>


            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                 
                                <span>
                                   <i class="glyphicon glyphicon-heart"></i>
                                </span>
                                <div class="media-body">
                         
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rutrum, urna eu pellentesque </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                
                                <span>
                                   <i class="glyphicon glyphicon-time"></i>
                                </span>
                                <div class="media-body">
                   
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rutrum, urna eu pellentesque </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                               
                                <span>
                                   <i class="glyphicon glyphicon-user"></i>
                                </span>
                                <div class="media-body">
                   
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rutrum, urna eu pellentesque </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>



    </div>
    <div class="section">
        <div class="container">
            <div class="section-title">
             
                <h3 style="color: #633991;
                            font-size: 33px;
                            font-weight: 500;">What our Customers Says</h3>
            </div>

            <div class="testimonials owl-carousel">
                <div class="testimonials-single">
                    <img src="../../images/taylor.jpg" alt="client" class="client-img">
                    <blockquote class="blockquote">Awsome site!</blockquote>
                    <h5 class="mt-4 mb-2">Taylor Swift</h5>
                    <p class="text-primary">United States</p>
                </div>
                <div class="testimonials-single">
                    <img src="../../images/taylor.jpg" alt="client" class="client-img">
                    <blockquote class="blockquote">Best place to Ordering food online!</blockquote>
                    <h5 class="mt-4 mb-2">Taylor Swift</h5>
                    <p class="text-primary">United States</p>
                </div>
                <div class="testimonials-single">
                    <img src="../../images/taylor.jpg" alt="client" class="client-img">
                    <blockquote class="blockquote">Amazing Site!</blockquote>
                    <h5 class="mt-4 mb-2">Taylor Swift</h5>
                    <p class="text-primary">United States</p>
                </div>
            </div>

        </div>

    </div>
  <footer class="saction8">
    <div class="container" id="contact">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3">
          <div class="site">
            <h3 style= "color:#00aaa7">Site Link</h3>
          </div>
          <div class="sitelink">
            <ul>
              <li>
                <span>&#10152;</span>
                <a href="#">About Us</a>
              </li>
              <li>
                <span>&#10152;</span>
                <a href="#">Contact Us</a>
              </li>
              <li>
                <span>&#10152;</span>
                <a href="#">Privacy Policy</a>
              </li>
              <li>
                <span>&#10152;</span>
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3">
          <div class="site">
            <h3 style= "color:#00aaa7">Site Link</h3>
          </div>
          <div class="sitelink">
            <ul>
              <li>
                <span>&#10152;</span>
                <a href="#">About Us</a>
              </li>
              <li>
                <span>&#10152;</span>
                <a href="#">Contact Us</a>
              </li>
              <li>
                <span>&#10152;</span>
                <a href="#">Privacy Policy</a>
              </li>
              <li>
                <span>&#10152;</span>
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3">
          <div class="follow">
            <h3 style= "color:#00aaa7">Follow Us On...</h3>
          </div>
          <div class="social">
            <ul>
              <li>
                <i class="fa fa-facebook-square"></i>
                <a href="#">Facebook</a>
              </li>
              <li>
                <i class="fa fa-twitter-square"></i>
                <a href="#">Twitter</a>
              </li>
              <li>
                <i class="fa fa-linkedin-square"></i>
                <a href="#">Linkedin</a>
              </li>
              <li>
                <i class="fa fa-google-plus-square"></i>
                <a href="#">Google Plus</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3">
          <div class="submit">
            <h3 style= "color:#00aaa7">Subscribe Newsletter</h3>
            <p>To get latest updates and food deals every week</p>
          </div>
          <div class="submitbox">
            <input type="text" />
            <div class="sub">
              <a href="#">Submit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <footer class="saction9">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="email">&copy; All right reserved 2018 atiidor </div>

        </div>
      </div>
    </div>
  </footer>
  <script type="text/javascript" src="js/sidemenu.js"></script>

</body>

</html>
