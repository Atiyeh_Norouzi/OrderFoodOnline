<!DOCTYPE html>
<html lang="en">
<head>
  
    <meta charset="UTF-8">
    <title>ATIIDOR</title>
    <link href='https://fonts.googleapis.com/css?family=Lobster+Two:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,300,700' rel='stylesheet' type='text/css' />
    <!--STYLESHEETS SHOULD BE LOADED HERE :)-->
    <link rel="stylesheet" href="css/atiyehnorouzzadeh.css">
    <link rel="stylesheet" href="css/dorsasamiee.css">
    <link rel="stylesheet" href="fonts/components-font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/ui-bootstrap-csp.css">
    <link rel="stylesheet"  href="css/font-awesome.css" />
    <link rel="stylesheet"  href="css/animate.css">
    <link rel="stylesheet"  href="css/responsive.css" />
    <!--STYLESHEETS FROM BOWER-->
</head> 

<body style="position: relative">
    <section class="main">
        <div class="main__inside" ui-view></div>
    </section>
    <!--DEPENDENCIES-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAK3nwDhmzXhT_7p5LP-s9JsSMWSDz9xYk&c&libraries=places&sensor=false"
        type="text/javascript"></script>

    <script src="js/scripts.js"></script>
    <script src="js/sidemenu.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script  src="js/jquery-1.11.3.min.js"></script>

    <script>
       window.location = "pages/home/home.php";
    </script>
</body>

</html>