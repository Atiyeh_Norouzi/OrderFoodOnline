
$(document).ready(function () {
    $(".hideProfile").hide();
    var hideFlag = true;
    function openProfile() {
        if (hideFlag)
            $(".hideProfile").show();
        else
            $(".hideProfile").hide();
        hideFlag = !hideFlag;
    }

    $("#login").click(openProfile);

});
function hideHiddens() {
    $(".hideProfile").hide();
}