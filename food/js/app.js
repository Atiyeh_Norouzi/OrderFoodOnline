angular.module('app', [
    'ui.bootstrap',
    'ui.router',
    'app.register',
    'app.codeexec',
    'app.home',
    'app.login',
    'uiGmapgoogle-maps'
])
    .config(function ($stateProvider, $urlRouterProvider, $logProvider, $locationProvider) {
        $logProvider.debugEnabled(true);
        $locationProvider.hashPrefix('!');
        // Now set up the states
        $stateProvider
            .state('home', {
                url: "/",
                controller: 'homeController',
                templateUrl: baseUrl + 'pages/home/home.php',
                controllerAs: 'vm'
            })
            .state('register', {
                url: "/register",
                controller: 'registerController',
                templateUrl: baseUrl + 'pages/register/register.php',
                controllerAs: 'vm'
            })
            .state('codeexec', {
                url: "/codeexec",
                controller: 'codeexecController',
                templateUrl: baseUrl + 'pages/register/code-exec.php',
                controllerAs: 'vm'
            })
            .state('login', {
                url: "/login",
                controller: 'loginController',
                templateUrl: baseUrl + 'pages/login/Login.php',
                controllerAs: 'vm'
            });
        $urlRouterProvider.otherwise('home');

    })
    .run(['$rootScope', '$state',
        function ($rootScope, $state) {
            $state.go('home');
        }]);
