// JavaScript Document


//animation effect(waypoint)
//paste this code under head tag or in a seperate js file.
// Wait for window load
$(window).load(function () {
  // Animate loader off screen
  $(".se-pre-con").fadeOut("slow");


  function onScrollInit(items, trigger) {
    items.each(function () {
      var osElement = $(this),
        osAnimationClass = osElement.attr('data-os-animation'),
        osAnimationDelay = osElement.attr('data-os-animation-delay');

      osElement.css({
        '-webkit-animation-delay': osAnimationDelay,
        '-moz-animation-delay': osAnimationDelay,
        'animation-delay': osAnimationDelay
      });

      var osTrigger = (trigger) ? trigger : osElement;

      osTrigger.waypoint(function () {
        osElement.addClass('animated').addClass(osAnimationClass);
      }, {
          triggerOnce: true,
          offset: '90%'
        });
    });
  }

  onScrollInit($('.os-animation'));
  onScrollInit($('.staggered-animation'), $('.staggered-animation-container')



  );
});

//smooth silding

$(function () {
  $('a[href*=#]:not([href=#])').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 110
        }, 2500);
        return false;
      }
    }
  });
  /*-----------------------------------
  * OWL CAROUSEL
  *-----------------------------------*/
  var $testimonialsDiv = $('.testimonials');
  if ($testimonialsDiv.length && $.fn.owlCarousel) {
    $testimonialsDiv.owlCarousel({
      items: 1,
      nav: true,
      dots: false,
      navText: ['<span class=""><i class="glyphicon glyphicon-chevron-left"></i></span>', '<span class=""><i class="glyphicon glyphicon-chevron-right"></i></span>']
    });
  }

  var $galleryDiv = $('.img-gallery');
  if ($galleryDiv.length && $.fn.owlCarousel) {
    $galleryDiv.owlCarousel({
      nav: false,
      center: true,
      loop: true,
      autoplay: true,
      dots: true,
      navText: ['<span class=""></span>', '<span class="></span>'],
      responsive: {
        0: {
          items: 1
        },
        768: {
          items: 3
        }
      }
    });
  }


});

